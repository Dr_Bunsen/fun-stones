package drBunsen;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class FunStones extends JavaPlugin {
	private String folderPath;
	private volatile int interval;
	private int[] block;
	private volatile boolean isActive;
	Thread workThread;
	
	
	public void onEnable() {
		folderPath = getDataFolder().getAbsolutePath() + "/";
		doIHaveAFolder();
		checkForConfig();
		loadConfig();
		isActive = false;
		workThread = new Worker();
	}
	
	public void onDisable() { 
		
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		
		if (cmd.getName().equalsIgnoreCase("funstones")) {
			if (isActive) {
				isActive = false;
				workThread = new Worker();
				sender.sendMessage("Stopped funstones for all worlds.");
			} else {
				workThread.start();
				isActive = true;
				sender.sendMessage("Activated funstones for all worlds.");
			}
		}
		
		return false;
		
	}
	
	
	@SuppressWarnings("deprecation")
	/**
	 * 
	 * The method that replaces the block underneath all the players in all the worlds with a randomblock out of the list given in the config.
	 * 
	 */
	void fun() {
		Random random = new Random();
		try {
				for (World w : getServer().getWorlds()) {
					for (Player p : w.getPlayers()) {
						Location l = new Location(w, p.getLocation().getX(), p.getLocation().getY() - 1, p.getLocation().getZ());
						Block b = w.getBlockAt(l);
						b.setTypeId(block[random.nextInt(block.length - 1)]);
					}
				}
		} catch (Exception s) {};
	}
	
	/**
	 * 
	 * In order to let the fun method be looped, we must make a new thread, or else bukkit would crash.
	 * This is that thread.
	 *
	 */
	class Worker extends Thread  {
		@SuppressWarnings("static-access")
		@Override
        public void run() {
			try {
				while(isActive) {
					fun();
					workThread.sleep(interval);
				}		
			} catch(Exception s) {}
		}
	}
	
	
	/**
	 * 
	 * Load the config file, processes it.
	 * 
	 */
	private void loadConfig() {
		String[] lines = readFille("config.yml").split("\n");
		for (String i : lines) {
			if (i.contains("interval")) {
				i = i.replace("interval:", "");
				i = i.replace(" ", "");
				this.interval = Integer.parseInt(i);
			} else if (i.contains("block_ids:")) {
				i = i.replace("block_ids:", "");
				i = i.replace(" ", "");
				String temp[] = i.split(",");
				block = new int[temp.length];
				for (int k = 0; k < temp.length; ++k) {
					block[k] = Integer.parseInt(temp[k]);
				}
			}
		}
	}
	
	
	/**
	 * 
	 * Check whether the plugin folder exists.
	 * If it doesn't, it creates it.
	 * 
	 * @return
	 * True if a folder existed, or does exist now. False if it couldn't create a folder.
	 * 
	 */
	private boolean doIHaveAFolder() {
		File f = loadFile("");
		if (f.exists()) {
			return true;
		} else {
			return f.mkdirs();
		}
	}
	
	/**
	 * 
	 * A simple file loader.
	 * 
	 * @param fileName
	 * The name of the file to be loaded from the plugin's folder.
	 * 
	 * @return
	 * The file if found, null if it wasn't found.
	 * 
	 */
	private File loadFile(String fileName) {
		return new File(folderPath + fileName);
	}
	
	/**
	 * 
	 * This checks whether there is a commands.yml.
	 * If there isn't, it will copy the standard commands.yml from the jar to the plugin folder.
	 * 
	 */
	void checkForConfig() {
		File f = loadFile("config.yml");
		if (!f.exists()) {
			URL inputUrl = getClass().getResource("/config.yml");
			try {
				FileUtils.copyURLToFile(inputUrl, f);
				log("We made the config.yml!");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * This outputs Strings in the console.
	 * 
	 * @param log
	 * String to be send to the console.
	 * 
	 */
	void log(String log) {
		System.out.println("[Information] " + log);
	}
	
	/**
	 * 
	 * Reads the given file from the plugin folder.
	 * 
	 * @param fileName
	 * The name of the wanted file.
	 * 
	 * @return
	 * The contents of the file in String format.
	 * 
	 */
	private String readFille(String fileName) {
		String output = "";
		File file = loadFile(fileName);
		
		if (file.exists()) {
			try {
				BufferedReader br = new BufferedReader(new FileReader(file));
				String line;
				while ((line = br.readLine()) != null) {
				   if (line.startsWith("#") || line.length() == 0) {
					   continue;
				   }
				   output = output + line + "\n";
				}
				br.close();
			} catch (Exception s) {
				s.printStackTrace();
			}
		}
		return output;
	}
	
}
